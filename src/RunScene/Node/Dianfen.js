Dianfen = cc.Node.extend({
	dianfen1:null,
	dianfen2:null,
	dianfen3:null,
	dianfen4:null,
	dianfen5:null,
	dianfen6:null,
	dianfen7:null,
	dianfen8:null,
	dianfen9:null,
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_DIANFEN_NODE);
		this.init();
	},

	init : function(){
		var dianfen =  new Button(this, 1, TAG_DIANFEN, "#touming.png",this.callback);
		dianfen.setPosition(cc.p(15,0));
		
		
		this.dianfen1 =new cc.Sprite("#dianfen.png");
		this.dianfen1.setPosition(cc.p(-10,0));
		this.addChild(this.dianfen1,2,TAG_DIANFEN1);
		
		this.dianfen2 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen2,2,TAG_DIANFEN2);
		this.dianfen2.setPosition(cc.p(0,1));
		
		this.dianfen3 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen3,2,TAG_DIANFEN3);
		this.dianfen3.setPosition(cc.p(10,2));
		
		this.dianfen4 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen4,2,TAG_DIANFEN4);
		this.dianfen4.setPosition(cc.p(20,4));
		
		this.dianfen5 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen5,2,TAG_DIANFEN5);
		this.dianfen5.setPosition(cc.p(30,7));
		
		this.dianfen6 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen6,2,TAG_DIANFEN6);
		this.dianfen6.setPosition(cc.p(40,10));
		
		this.dianfen7 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen7,2,TAG_DIANFEN7);
		this.dianfen7.setPosition(cc.p(20,-5));
		
		this.dianfen8 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen8,2,TAG_DIANFEN8);
		this.dianfen8.setPosition(cc.p(30,-10));

		this.dianfen9 =new cc.Sprite("#dianfen.png");
		this.addChild(this.dianfen9,2,TAG_DIANFEN9);
		this.dianfen9.setPosition(cc.p(40,-15));
		
		this.addlabel(this,"淀粉",cc.p(0,20),TAG_LABEL);

	},
	addlabel:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
	},
	addlabel1:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname1:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
	},
	move1:function(time){
		
		this.dianfen2.runAction(cc.moveTo(time,cc.p(-2,6)));
		this.dianfen3.runAction(cc.moveTo(time,cc.p(1,-6)));
		this.dianfen8.runAction(cc.moveTo(time,cc.p(25,-15)));
		this.dianfen9.runAction(cc.moveTo(time,cc.p(30,-25)));		
	},
	
	move2:function(time){
		this.dianfen3.runAction(cc.moveTo(time,cc.p(-5,-15)));
		this.dianfen7.runAction(cc.moveTo(time,cc.p(5,-8)));
		this.dianfen4.runAction(cc.moveTo(time,cc.p(15,4)));
	},
	move3:function(time){
		this.dianfen1.runAction(cc.moveTo(time,cc.p(-15,0)));
		this.dianfen5.runAction(cc.moveTo(time,cc.p(25,15)));
		this.dianfen6.runAction(cc.moveTo(time,cc.p(28,0)));
		this.dianfen7.runAction(cc.moveTo(time,cc.p(5,-5)));
		this.dianfen8.runAction(cc.moveTo(time,cc.p(18,-15)));
	},
	addarrow:function(str,pos,rotate,scalex,scaley){
		var arrow = new cc.Sprite(str);
		arrow.setScale(scalex,scaley);
		arrow.setRotation(rotate);
		arrow.setPosition(pos);
		ll.run.addChild(arrow,2);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1),cc.callFunc(function(){
			arrow.removeFromParent(true);
		},this));
		arrow.runAction(seq);	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_DIANFEN:
			var seq = cc.sequence(cc.callFunc(function(){
				var label = this.getChildByTag(TAG_LABEL);
				label.setOpacity(0);				
			},this),cc.moveTo(2,cc.p(550,495)),cc.callFunc(function(){
				this.move1(1);
				this.addarrow("#arrow1.png",cc.p(470,515),0,0.4,0.3);
				this.addlabel1(ll.run,"唾液",cc.p(560,515),TAG_LABEL2);
				this.addlabel1(ll.run,"麦芽糖",cc.p(560,460),TAG_LABEL3);
				},this),cc.delayTime(2),cc.moveTo(4,cc.p(550,270)),cc.callFunc(function(){
					this.addarrow("#arrow1.png",cc.p(470,265),-20,0.4,0.3);
					this.addarrow("#arrow2.png",cc.p(730,305),170,0.6,0.3);
					this.move2(1);
					this.setname(ll.run,"肠液",cc.p(550,290),TAG_LABEL2);
					this.setname(ll.run,"胰液",cc.p(610,290),TAG_LABEL3);
				},this),cc.delayTime(2),cc.moveTo(2,cc.p(550,180)),cc.callFunc(function(){
					this.move3(1);
					this.setname1(ll.run,"葡萄糖",cc.p(560,140),TAG_LABEL2);
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.flowNext();
				},this)
				);
			this.runAction(seq);
			break;

		}
	},
	

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_DIANFEN:
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});