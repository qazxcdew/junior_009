Zhifang = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_ZHIFANG_NODE);
		this.init();
	},

	init : function(){
		var zhifang =  new Button(this, 1, TAG_ZHIFANG, "#touming.png",this.callback);
		zhifang.setPosition(cc.p(15,0));
		
		this.zhifang1 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang1,2,TAG_ZHIFANG1);
		this.zhifang1.setPosition(cc.p(-10,0));
		this.zhifang11 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang11,2,TAG_ZHIFANG11);
		this.zhifang11.setPosition(cc.p(-10,1));
		
		this.zhifang2 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang2,2,TAG_ZHIFANG2);
		this.zhifang2.setPosition(cc.p(2,0));
		this.zhifang12 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang12,2,TAG_ZHIFANG12);
		this.zhifang12.setPosition(cc.p(2,1));
		
		this.zhifang3 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang3,2,TAG_ZHIFANG3);
		this.zhifang3.setPosition(cc.p(13,0));
		this.zhifang13 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang13,2,TAG_ZHIFANG13);
		this.zhifang13.setPosition(cc.p(13,1));
		
		this.zhifang4 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang4,2,TAG_ZHIFANG4);
		this.zhifang4.setPosition(cc.p(24,0));
		this.zhifang14 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang14,2,TAG_ZHIFANG14);
		this.zhifang14.setPosition(cc.p(24,1));
		
		this.zhifang5 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang5,2,TAG_ZHIFANG5);
		this.zhifang5.setPosition(cc.p(-5,-10));
		this.zhifang15 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang15,2,TAG_ZHIFANG15);
		this.zhifang15.setPosition(cc.p(-5,-9));
		
		this.zhifang6 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang6,2,TAG_ZHIFANG6);
		this.zhifang6.setPosition(cc.p(6,-10));
		this.zhifang16 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang16,2,TAG_ZHIFANG16);
		this.zhifang16.setPosition(cc.p(6,-9));
		
		this.zhifang7 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang7,2,TAG_ZHIFANG7);
		this.zhifang7.setPosition(cc.p(20,-10));
		this.zhifang17 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang17,2,TAG_ZHIFANG17);
		this.zhifang17.setPosition(cc.p(20,-9));
		
		this.zhifang8 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang8,2,TAG_ZHIFANG8);
		this.zhifang8.setPosition(cc.p(-20,-30));
		this.zhifang18 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang18,2,TAG_ZHIFANG18);
		this.zhifang18.setPosition(cc.p(-20,-29));
		
		this.zhifang9 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang9,2,TAG_ZHIFANG9);
		this.zhifang9.setPosition(cc.p(-10,-25));
		this.zhifang19 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang19,2,TAG_ZHIFANG19);
		this.zhifang19.setPosition(cc.p(-10,-24));
		
		this.zhifang10 = new cc.Sprite("#zhifang1.png"); 
		this.addChild(this.zhifang10,2,TAG_ZHIFANG10);
		this.zhifang10.setPosition(cc.p(2,-22));
		this.zhifang20 = new cc.Sprite("#zhifang2.png"); 
		this.addChild(this.zhifang20,2,TAG_ZHIFANG20);
		this.zhifang20.setPosition(cc.p(2,-21));
		
		this.addlabel(this,"脂肪",cc.p(0,20),TAG_LABEL);

	},
	addlabel:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
	},
	addlabel1:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname1:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
	},
	move1:function(time){
		
        this.zhifang1.runAction(cc.spawn(cc.moveTo(time,cc.p(-15,10)),cc.rotateTo(time,30)));
        this.zhifang11.runAction(cc.moveTo(time,cc.p(-10,10)));
        this.zhifang2.runAction(cc.moveTo(time,cc.p(5,5)));
        this.zhifang12.runAction(cc.spawn(cc.moveTo(time,cc.p(10,15)),cc.rotateTo(time,30)));
        
        this.zhifang5.runAction(cc.moveTo(time,cc.p(-15,-5)));
        this.zhifang15.runAction(cc.moveTo(time,cc.p(-15,-4)));
        this.zhifang6.runAction(cc.spawn(cc.moveTo(time,cc.p(0,-8)),cc.rotateTo(time,-60)));
        
        this.zhifang4.runAction(cc.moveTo(time,cc.p(30,0)));
        this.zhifang14.runAction(cc.moveTo(time,cc.p(30,1)));
        
        this.zhifang10.runAction(cc.spawn(cc.moveTo(time,cc.p(5,-22)),cc.rotateTo(time,50)));
        this.zhifang20.runAction(cc.spawn(cc.moveTo(time,cc.p(10,-25)),cc.rotateTo(time,30)));
        
        this.zhifang17.runAction(cc.spawn(cc.moveTo(time,cc.p(25,-15)),cc.rotateTo(time,-30)));
        

	},

	move2:function(time){
	
	},
	move3:function(time){
		this.zhifang4.runAction(cc.moveTo(time,cc.p(30,0)));
		this.zhifang14.runAction(cc.moveTo(time,cc.p(30,1)));

		this.zhifang13.runAction(cc.spawn(cc.moveTo(time,cc.p(15,1)),cc.rotateTo(time,-30)));
		this.zhifang4.runAction(cc.spawn(cc.moveTo(time,cc.p(28,5)),cc.rotateTo(time,-30)));

		this.zhifang5.runAction(cc.spawn(cc.moveTo(time,cc.p(-20,-5)),cc.rotateTo(time,-45)));
		this.zhifang8.runAction(cc.spawn(cc.moveTo(time,cc.p(-25,-25)),cc.rotateTo(time,45)));
		this.zhifang19.runAction(cc.spawn(cc.moveTo(time,cc.p(-5,-30)),cc.rotateTo(time,-20)));

	},
	addarrow:function(str,pos,rotate,scalex,scaley){
		var arrow = new cc.Sprite(str);
		arrow.setScale(scalex,scaley);
		arrow.setRotation(rotate);
		arrow.setPosition(pos);
		ll.run.addChild(arrow,2);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1),cc.callFunc(function(){
			arrow.removeFromParent(true);
		},this));
		arrow.runAction(seq);	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_ZHIFANG:
			var seq = cc.sequence(cc.callFunc(function(){
				var label = this.getChildByTag(TAG_LABEL);
				label.setOpacity(0);
			},this),cc.moveTo(3,cc.p(715,350)),cc.callFunc(function(){
				this.move1(1);
				this.addarrow("#arrow2.png",cc.p(580,355),-10,0.8,0.3);
				this.addlabel1(ll.run,"胆汁",cc.p(725,380),TAG_LABEL6);
				this.addlabel1(ll.run,"脂肪颗粒",cc.p(725,305),TAG_LABEL7);
			},this),cc.delayTime(2),cc.moveTo(1.5,cc.p(715,260)),cc.callFunc(function(){
				this.move2(1);
				this.addarrow("#arrow2.png",cc.p(575,270),-10,0.7,0.3);
				this.addarrow("#arrow1.png",cc.p(820,315),160,0.4,0.3);
				this.setname(ll.run,"肠液",cc.p(705,290),TAG_LABEL6);
				this.setname(ll.run,"胰液",cc.p(750,290),TAG_LABEL7);
			},this),cc.delayTime(2),cc.moveTo(2,cc.p(715,185)),cc.callFunc(function(){
				this.move3(1);
				this.setname1(ll.run,"脂肪酸",cc.p(750,140),TAG_LABEL6);
				this.setname1(ll.run,"甘油",cc.p(695,140),TAG_LABEL7);
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.flowNext();
			},this));
			this.runAction(seq);
			break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});