Danbaizhi = cc.Node.extend({
	danbaizhi1:null,
	danbaizhi2:null,
	danbaizhi3:null,
	danbaizhi4:null,
	danbaizhi5:null,
	danbaizhi6:null,
	danbaizhi7:null,
	danbaizhi8:null,
	danbaizhi9:null,
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_DANBAIZHI_NODE);
		this.init();
	},

	init : function(){	
		var danbaizhi =  new Button(this, 1, TAG_DANBAIZHI, "#touming.png",this.callback);
		danbaizhi.setPosition(cc.p(15,0));
		
		this.danbaizhi1 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi1,2,TAG_DANBAIZHI1);
		this.danbaizhi1.setPosition(cc.p(-10,0));
		
		this.danbaizhi2 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi2,2,TAG_DANBAIZHI2);
		this.danbaizhi2.setPosition(cc.p(0,1));
		
		this.danbaizhi3 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi3,2,TAG_DANBAIZHI3);
		this.danbaizhi3.setPosition(cc.p(10,2));
		
		this.danbaizhi4 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi4,2,TAG_DANBAIZHI4);
		this.danbaizhi4.setPosition(cc.p(20,3));
		
		this.danbaizhi5 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi5,2,TAG_DANBAIZHI5);
		this.danbaizhi5.setPosition(cc.p(-5,-10));
		
		this.danbaizhi6 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi6,2,TAG_DANBAIZHI6);
		this.danbaizhi6.setPosition(cc.p(5,-10));
		

		this.danbaizhi7 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi7,2,TAG_DANBAIZHI7);
		this.danbaizhi7.setPosition(cc.p(0,-20));
		

		this.danbaizhi8 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi8,2,TAG_DANBAIZHI8);
		this.danbaizhi8.setPosition(cc.p(10,-20));
		
		this.danbaizhi9 = new cc.Sprite("#danbaizhi.png"); 
		this.addChild(this.danbaizhi9,2,TAG_DANBAIZHI9);
		this.danbaizhi9.setPosition(cc.p(20,-25));
		
		this.addlabel(this,"蛋白质",cc.p(0,20),TAG_LABEL);

	},
	addlabel:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
	},
	addlabel1:function(obj,name,pos,tag){
		var label = new cc.LabelTTF(name,"微软雅黑",17);
		label.setColor(cc.color(0, 0, 0));
		label.setPosition(pos);
		obj.addChild(label,1,tag);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1));
		label.runAction(seq);
	},
	setname1:function(obj,name,pos,tag){
		var label = obj.getChildByTag(tag);
		label.setString(name);
		label.setOpacity(250);
		label.setPosition(pos);
	},
	move1:function(time){
	
		this.danbaizhi3.runAction(cc.moveTo(time,cc.p(15,2)));
		this.danbaizhi4.runAction(cc.moveTo(time,cc.p(25,3)));
		this.danbaizhi6.runAction(cc.moveTo(time,cc.p(20,-8)));
		this.danbaizhi7.runAction(cc.moveTo(time,cc.p(10,-30)));

	},

	move2:function(time){
		this.danbaizhi6.runAction(cc.moveTo(time,cc.p(15,-12)));
		this.danbaizhi8.runAction(cc.moveTo(time,cc.p(5,-16)));
		this.danbaizhi5.runAction(cc.moveTo(time,cc.p(-10,-15)));
	},
	move3:function(time){
		this.danbaizhi1.runAction(cc.moveTo(2,cc.p(-15,0)));
		this.danbaizhi4.runAction(cc.moveTo(2,cc.p(30,0)));
		this.danbaizhi5.runAction(cc.moveTo(2,cc.p(-12,-15)));
		this.danbaizhi6.runAction(cc.moveTo(2,cc.p(18,-12)));
		this.danbaizhi7.runAction(cc.moveTo(2,cc.p(5,-30)));
		this.danbaizhi9.runAction(cc.moveTo(2,cc.p(18,-25)));
	},
	addarrow:function(str,pos,rotate,scalex,scaley){
		var arrow = new cc.Sprite(str);
		arrow.setScale(scalex,scaley);
		arrow.setRotation(rotate);
		arrow.setPosition(pos);
		ll.run.addChild(arrow,2);
		var seq = cc.sequence(cc.delayTime(2),cc.fadeOut(1),cc.callFunc(function(){
			arrow.removeFromParent(true);
		},this));
		arrow.runAction(seq);	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_DANBAIZHI:
			var seq = cc.sequence(cc.callFunc(function(){
				var label = this.getChildByTag(TAG_LABEL);
				label.setOpacity(0);				
			},this),cc.moveTo(3,cc.p(635,400)),cc.callFunc(function(){
				this.move1(1);
				this.addarrow("#arrow2.png",cc.p(550,400),-10,0.5,0.3);
				this.addlabel1(ll.run,"胃液",cc.p(645,420),TAG_LABEL4);
				this.addlabel1(ll.run,"多肽",cc.p(645,355),TAG_LABEL5);
			},this),cc.delayTime(2),cc.moveTo(3,cc.p(635,270)),cc.callFunc(function(){
				this.move2(1);
				this.addarrow("#arrow2.png",cc.p(540,275),-10,0.4,0.3);
				this.addarrow("#arrow2.png",cc.p(760,305),170,0.4,0.3);
				this.setname(ll.run,"肠液",cc.p(620,290),TAG_LABEL4);
				this.setname(ll.run,"胰液",cc.p(680,290),TAG_LABEL5);
			},this),cc.delayTime(2),cc.moveTo(2,cc.p(635,185)),cc.callFunc(function(){
				this.move3(1);
				this.setname1(ll.run,"氨基酸",cc.p(635,140),TAG_LABEL4);
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.flowNext();
			},this));
			this.runAction(seq);
			break;

		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_DANBAIZHI:
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});