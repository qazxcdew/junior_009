var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 
		
		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		//试管架
	var bg = new cc.Sprite("#bg.png");
	bg.setPosition(cc.p(640,350));
	bg.setScale(1.1);
	this.addChild(bg);
	
	var shiwu = new Button(this, 2, TAG_SHIWU, "#shiwu.png",this.callback);
	shiwu.setPosition(cc.p(410,570));
				


	},

	

	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},
	
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_SHIWU:
			var seq = cc.sequence(cc.moveBy(2,cc.p(135,0)),cc.callFunc(function(){
				var dianfen = new Dianfen(ll.run);
				dianfen.setPosition(cc.p(550,565));
				dianfen.setOpacity(0);				
				dianfen.runAction(cc.fadeIn(1));
			},this),cc.moveBy(1,cc.p(80,0)),cc.callFunc(function(){
				var danbaizhi = new Danbaizhi(ll.run);
				danbaizhi.setPosition(cc.p(635,565));
				danbaizhi.setOpacity(0);				
				danbaizhi.runAction(cc.fadeIn(1));
			},this),cc.moveBy(1,cc.p(80,0)),cc.callFunc(function(){
				var zhifang = new Zhifang(ll.run);
				zhifang.setPosition(cc.p(715,565));
				zhifang.setOpacity(0);				
				zhifang.runAction(cc.fadeIn(1));
			},this),cc.fadeOut(0.5),cc.callFunc(function(){
				this.flowNext();
			},this));
			p.runAction(seq);
			break;
		 
		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});